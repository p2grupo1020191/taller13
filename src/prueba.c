#include "slaballoc.h"
#include "objetos.h"
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
typedef struct argumentos{
	SlabAlloc *slab;
	int id_hilo;
	
} datos;
sem_t mut;
void *tarea_hilos(void *args){
	datos *argumentos = (datos *)args;
	printf("Inicio hilo %d \n",argumentos->id_hilo);
	void *objetos[1000];
	for(int h=0;h<1000;h++){
		
		//sem_wait(&mut);
		void *obj = obtener_cache(argumentos->slab,0);
		//printf("Obtener cache  #%d del hilo %d al slab %p\n",h,argumentos->id_hilo,obj);	
		objeto_ejemplo *objtaller = (objeto_ejemplo *)obj;
		objtaller->id = argumentos->id_hilo;
		objtaller->data[0] = objtaller->id * 0;
		objtaller->data[1] = objtaller->id * 1;
		objtaller->data[2] = objtaller->id * 2;
		
		objetos[h] = obj;
		//sem_post(&mut);
	}
	printf("Devolviendo cache hilo %d \n",argumentos->id_hilo);
	for(int h=0;h<300;h++){
		//sem_wait(&mut);	
		devolver_cache(argumentos->slab,objetos[700+h]);
		//sem_post(&mut);
	}
	printf("Verificando disponibilidad hilo %d\n",argumentos->id_hilo);
	for(int k=0;k<700;k++){
		//sem_wait(&mut);
		objeto_ejemplo *cobjeto = (objeto_ejemplo *) objetos[k];
		//printf("Esta viendo los objetos dentro del arreglo: \n");
        	if(cobjeto->id != argumentos->id_hilo || cobjeto->data[0] != argumentos->id_hilo * 0 || cobjeto->data[1] != argumentos->id_hilo * 1|| cobjeto->data[2] != argumentos->id_hilo * 2){
			printf("Id a mostrar %d, en el arreglo %d \n",argumentos->id_hilo, cobjeto->id);
			printf("id %d, campo data[0] %d\n",argumentos->id_hilo*0, cobjeto->data[0]);
			printf("id %d, campo data[1] %d\n",argumentos->id_hilo*1, cobjeto->data[1]);
			printf("id %d, campo data[2] %d\n",argumentos->id_hilo*2, cobjeto->data[2]);
		}
            	//sem_post(&mut);
	}
	printf("Hilo %d fin\n",argumentos->id_hilo);
	return NULL;

}

int main(int argc, char *argv[]){
	printf("Direccion hilo main %ld \n",pthread_self());
	if(argc != 3){
		printf("Pruebe con: ./bin/prueba -n <# DE HILOS>\n");
		return -1;
	}
	int hilos = atoi(argv[2]);
	printf("Numero de hilos: %d\n",hilos);
	if(hilos <= 0){
		printf("Numero de hilos debe ser mayor a 0 \n");
		return -1;
	}
	
	constructor_fn c = crear_objeto_ejemplo;
	destructor_fn dest = destruir_objeto_ejemplo;
	
	SlabAlloc *sl = crear_cache("Taller 13",sizeof(objeto_ejemplo),c,dest);
	datos data[hilos];	
	for(int x=0;x<hilos;x++){
		data[x].slab = sl;
		data[x].id_hilo = x;	
	}
	pthread_t ids[hilos];
	/*if(sem_init(&mut, 0, 1) != 0){
		perror("Error al iniciar el semaforo");	
	}*/
	for(int i=0;i<hilos;i++){
		pthread_create(&ids[i],NULL,tarea_hilos,&data[i]);
	}
	
	for(int j=0;j<hilos;j++){
		pthread_join(ids[j],NULL);
	}
	
	destruir_cache(sl);
	printf("se destruyo el cache\n");
	
	

	return 0;
}


