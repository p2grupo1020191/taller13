#include <stdlib.h>
#include <stdio.h>
#include "objetos.h"
#include "slaballoc.h"
#include <semaphore.h>

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	//inicializacion de las vatiables del SlabAlloc
	SlabAlloc *slaballoc = (SlabAlloc *)malloc(sizeof(SlabAlloc));
	slaballoc->nombre = nombre;
	slaballoc->tamano_cache = TAMANO_CACHE;
	slaballoc->tamano_objeto = tamano_objeto;
	slaballoc->constructor = constructor;
	slaballoc->destructor = destructor;
	slaballoc->cantidad_en_uso = 0;
	slaballoc->slab_ptr= (Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);
	slaballoc->mem_ptr = malloc(TAMANO_CACHE*tamano_objeto);


	//incializacion de todos los objetos y slabs
	if(slaballoc->slab_ptr != NULL && slaballoc->mem_ptr!=NULL){
		int i;
		for(i=0;i<slaballoc->tamano_cache;i++){
			slaballoc->constructor(slaballoc->mem_ptr+(i*slaballoc->tamano_objeto),tamano_objeto);
			slaballoc->slab_ptr[i].ptr_data = slaballoc->mem_ptr+(i*slaballoc->tamano_objeto);
			slaballoc->slab_ptr[i].ptr_data_old = NULL;
			slaballoc->slab_ptr[i].status = DISPONIBLE;
			if(sem_init(&(slaballoc->slab_ptr[i].mutex), 0, 1) != 0){
				perror("Error al iniciar el semaforo");	
			}
		}
		return slaballoc;
	}else{
		return NULL;
	}
}

void *obtener_cache(SlabAlloc *alloc, int crecer){
	if(alloc != NULL){
		if((alloc->cantidad_en_uso == alloc->tamano_cache)){//para ver si todos los slabs estan en uso o si hay disponibles

			if(crecer == 1){
				//se hace crecer el cache al doble de su tamanio
				alloc->mem_ptr = realloc(alloc->mem_ptr,(alloc->tamano_objeto*alloc->tamano_cache*2));//se duplica la memoria a la que apunta mem_ptr
				alloc->slab_ptr = realloc(alloc->slab_ptr,sizeof(Slab)*alloc->tamano_cache*2);// se duplica la memoria a la que apunta slab_ptr
				int i;
				for(i=0;i<alloc->tamano_cache*2;i++){//recorre la nueva region de memmoria asignada
					if(i<alloc->tamano_cache && alloc->slab_ptr[i].ptr_data_old == NULL ){
						alloc->slab_ptr[i].ptr_data_old = alloc->slab_ptr[i].ptr_data;
					}
					if(i >= alloc->tamano_cache){
						alloc->constructor(alloc->mem_ptr+(i*alloc->tamano_objeto),alloc->tamano_objeto);//se inicialiazan los nuevos objetos
						alloc->slab_ptr[i].status = DISPONIBLE;// se los marca como disponible
						alloc->slab_ptr[i].ptr_data_old = NULL;
					}
					alloc->slab_ptr[i].ptr_data = alloc->mem_ptr+(i*alloc->tamano_objeto);//se hace que apunten a la region de cada uno de los objetos
				}
				alloc->tamano_cache = alloc->tamano_cache*2;
				alloc->cantidad_en_uso++;
				alloc->slab_ptr[alloc->tamano_cache/2].status=EN_USO;
				return alloc->mem_ptr+(alloc->tamano_objeto*alloc->tamano_cache/2);
			}else{
				return NULL;
			}
		}else{// hay slabs disponibles
			int i;
			for(i=0;i<alloc->tamano_cache;i++){
				
				sem_wait(&(alloc->slab_ptr[i].mutex));				
				if(alloc->slab_ptr[i].status == DISPONIBLE){
										
					alloc->cantidad_en_uso++;
					alloc->slab_ptr[i].status = EN_USO;
					sem_post(&(alloc->slab_ptr[i].mutex));
					return 	alloc->slab_ptr[i].ptr_data;
				}
				sem_post(&(alloc->slab_ptr[i].mutex));
			}
		}
	}

	return NULL;
}


void devolver_cache(SlabAlloc *alloc, void *obj){
	if(obj != NULL && alloc != NULL){
		int i;
		for(i =0;i<alloc->tamano_cache;i++){
			
			sem_wait(&(alloc->slab_ptr[i].mutex));		
			if(obj == alloc->slab_ptr[i].ptr_data){
				
				alloc->slab_ptr[i].status = DISPONIBLE;
				alloc->cantidad_en_uso--;

			}
			sem_post(&(alloc->slab_ptr[i].mutex));
		}
	}
}

void destruir_cache(SlabAlloc *cache){
	if(cache != NULL && cache->cantidad_en_uso == 0){

		for(int i = 0; i<cache->tamano_cache; i++){
			cache->destructor(cache->mem_ptr+(i*cache->tamano_objeto),cache->tamano_objeto);
		}
		free(cache->slab_ptr);
		free(cache->mem_ptr);
		free(cache);
		cache->mem_ptr = NULL;
		cache->nombre = NULL;
		cache->slab_ptr = NULL;
		cache = NULL;





	}
}

void stats_cache(SlabAlloc *cache){
	if(cache != NULL){
		printf("Nombre de cache: %s\n",cache->nombre);
		printf("Cantidad de slabs: %d\n",cache->tamano_cache );
		printf("Cantidad de slabs en uso: %d\n",cache->cantidad_en_uso );
		printf("Tamano de objeto: %ld\n",cache->tamano_objeto );
		printf("\n");
		printf("Direccion de cache->mem_ptr: %p\n", cache->mem_ptr);
		for(int i=0;i<cache->tamano_cache;i++){
			printf("direccion ptr[%d].ptr_data: %p ",i,cache->slab_ptr[i].ptr_data);
			if(cache->slab_ptr[i].status == EN_USO ){
				printf("EN_USO");
			}else{
				printf("DISPONIBLE");
			}
			printf(", prt[%d].ptr_data_old: %p\n",i,cache->slab_ptr[i].ptr_data_old );
		}

	}else{
		printf("El puntero SlabAlloc apunta a NULL \n");
	}
}
