
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/slaballoc.o
	gcc -Wall -pthread -g obj/prueba.o obj/Ejemplo.o obj/slaballoc.o -o bin/prueba	
obj/prueba.o: src/prueba.c
	gcc -Wall -c -I include/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -I include/ src/Ejemplo.c -o obj/Ejemplo.o

obj/slaballoc.o: src/slaballoc.c
	gcc -Wall -c -I include/ src/slaballoc.c -o obj/slaballoc.o


.PHONY: clean
clean:
	rm bin/* obj/*
